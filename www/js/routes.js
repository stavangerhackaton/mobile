angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('tabsController', {
      url: '/page1',
      templateUrl: 'templates/tabsController.html',
      abstract:true
    })
    .state('tabsController.overviewDetails', {
    url: '/overviewDetails/:caseId',
    views: {
      'tab1': {
        templateUrl: 'js/Components/OverviewDetails/overviewDetails.html',
        controller: 'OverviewDetailsController'
      }
    }
  })
    .state('tabsController.overview', {
    url: '/overview',
      views: {
        'tab1': {
          templateUrl: 'js/Components/Overview/overview.html',
          controller: 'OverviewController'
        }
      }
    })

    .state('tabsController.map', {
    url: '/map',
      views: {
        'tab1': {
          templateUrl: 'js/Components/Map/map.html',
          controller: 'MapController'
        }
      }
    })

    .state('tabsController.mapCase', {
    url: '/map/:caseId',
      views: {
        'tab1': {
          templateUrl: 'js/Components/Map/map.html',
          controller: 'MapController'
        }
      }
    })



    .state('tabsController.registerCase', {
      url: '/newticket',
      views: {
        'tab2': {
          templateUrl: 'js/Components/RegisterCase/registerCase.html',
          controller: 'RegisterCaseController'
        }
      }
    })



    .state('tabsController.register', {
    url: '/signup',
    views: {
      'tab3': {
        templateUrl: 'js/Components/RegisterProfile/Register.html',
        controller: 'RegisterController'
      }
    }
  })
    .state('tabsController.login', {
    url: '/login',
    views: {
      'tab3': {
        templateUrl: 'js/Components/Login/Login.html',
        controller: 'LoginController'
      }
    }
  })
    .state('tabsController.overviewDetailsProfile', {
    url: '/overviewDetailsProfile/:caseId',
    views: {
      'tab3': {
        templateUrl: 'js/Components/OverviewDetails/overviewDetails.html',
        controller: 'OverviewDetailsController'
      }
    }
  })
    .state('tabsController.profile', {
    url: '/profile',
    views: {
      'tab3': {
        templateUrl: 'js/Components/Profile/Profile.html',
        controller: 'ProfileController'
      }
    }
  });


$urlRouterProvider.otherwise('/page1/overview')



});
