(function() {
  angular.module('app.login')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$scope','UserService','$state'];

  function LoginController($scope,UserService,$state) {
    $scope.vm = {
      email: '',
      password: ''
    };

    $scope.error == '';


    $scope.login = function() {
      UserService.login($scope.vm.email,$scope.vm.password,loginSuccess,loginFailed);

    };

    function loginSuccess() {
      $state.go('tabsController.profile');
    }

    function loginFailed(error) {
      $scope.error = error;
    }

  }


})();


