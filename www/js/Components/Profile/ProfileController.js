(function() {
  angular.module('app.profile')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['$scope','$state', '$q', 'UserService', '$ionicLoading','$timeout','CaseService'];

  function ProfileController($scope, $state, $q, UserService, $ionicLoading,$timeout,CaseService) {
    console.log('ProfileController loaded');



    $scope.$on('$ionicView.beforeEnter', function() {
      console.log('View will enter');
      UserService.getLoggedInUser(userIsLoggedIn,userIsLoggedOut);
      $scope.user;

      $scope.loggedIn = false;

      $scope.cases = [];
    })


    function userIsLoggedIn(user) {
      $timeout(function() {
        $scope.loggedIn = true;
        $scope.user = _.assign({},user);
        $scope.cases = CaseService.getCasesForUser(user.id);
      });
    }

    function userIsLoggedOut(error) {
      $timeout(function() {
        $scope.loggedIn = false;
      });
    }

    var user = UserService.getUser();
    if (typeof user !== 'undefined' && user.name) {
      $scope.loggedIn = true;
      $scope.name = user.name;
    }
  }


})();


