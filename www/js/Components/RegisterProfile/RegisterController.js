(function() {
  angular.module('app.register')
    .controller('RegisterController', RegisterController);

  RegisterController.$inject = ['$scope','UserService','$state'];

  function RegisterController($scope,UserService,$state) {
    $scope.vm = {
      name: '',
      email: '',
      password: ''
    };

    $scope.error = '';


    $scope.register = function() {
      UserService.registerUser({
        'name': $scope.vm.name,
        'password': $scope.vm.password,
        'email': $scope.vm.email
      }, registrationSuccessfull,registrationFailed);



    }

    function registrationSuccessfull() {
      $state.go('tabsController.profile');
    }

    function registrationFailed(error) {
      $scope.error = error;
    }

  }


})();


