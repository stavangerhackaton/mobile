(function() {

  angular.module('app.shared')
    .service('UserService', UserService);

  UserService.$inject = [];

  function UserService() {
    // For the purpose of this example I will store user data on ionic local storage but you should save it on a database

    var key = 'users';

    var base = {
      id: 0,
      score: 0,
      name: '',
      email: '',
      password: ''
    };

    /**
     * Registrer bruker
     * @param   {object}   user_data Brukerdata
     * @param   {function} success   Callback for suksess
     * @param   {function} error     Callback ved feil
     */
   function registerUser(user_data,success,error) {
      //check if user exists
      var user = getUsers().filter(function(user) {
        console.log(user);
        return user.email === user_data.email;
      });

      if (user.length > 0) {
        error('E-post er allerede i bruk');
        return;
      }


      user_data.id = Date.now();
      user_data.score = 0;
      var newUser = _.assign({},base,user_data);
      addUser(newUser);
      setLoggedInUser(newUser.id);
      typeof(success) === 'function' ? success() : '';
    };

    /**
     * Login the user
     * @param   {string}   email    Brukerens e-post
     * @param   {string}   password Brukerens passord
     * @param   {function} success  Callback for suksess
     * @param   {function} error    Callback for feil
     */
    function login(email, password,success,error) {
      var users = null;
      users = getUsers().filter(function(user) {
        return user.password == password && user.email == email;
      })[0];

      if (typeof(users) === 'undefined' || users.length <= 0) {
        error("E-post eller passord er feil");
        return;
      }
      setLoggedInUser(users.id);
      success();
    }

    /**
     * Get all users from the database
     *
     * @returns {Array} List of users
     */

    function getUsers() {
      return JSON.parse(window.localStorage.getItem(key) || '[]');
    }


    /**
     * Add a user to the database
     * @param {object} user User object
     */

    function addUser(user) {
      var users = getUsers();
      users.push(user);
      window.localStorage.setItem(key,JSON.stringify(users));
    }

    function addPointsToUser(userId,points) {
      var user = getUser(userId);
      user.score += points;
      return updateUser(user);

    }

    function updateUser(user) {
      var newUser = _.assign({},base,user);
      var users = getUsers().filter(function(user) {
        return user.id != newUser.id;
      });

      users.push(newUser);
      saveUserList(users);
      return newUser;

    }

    function saveUserList(users) {
      window.localStorage.setItem(key,JSON.stringify(users));
    }





    /**
     * Lagre innlogget bruker
     * @param {object} user_data brukerdata
     */

    function setLoggedInUser(userId) {
      console.log(userId);
      window.localStorage.loggedInUser = userId;
    };

    /**
     * Get the currently logged in user
     * @param   {function} success SuccessCallback
     * @param   {function} error   ErrorCallback
     */

    function getLoggedInUser(success,error) {
      var userId = window.localStorage.getItem('loggedInUser');
      if (!userId) {
        console.log('BrukerID er ikke satt');
        typeof(error) === 'function' ? error('Bruker er ikke logget inn') : '';
        return;
      }

      typeof(success) === 'function' ? success(getUsers().filter(function(user) {
        console.log(user,user.id == userId);
        return user.id == userId;
      })[0]) : '';
    }

    /**
     * Get a user based on the ID
     * @param   {number} id User ID
     * @returns {object} USER
     */
    function getUser(id){
      return getUsers().filter(function(user) {
        return user.id == id;
      })[0];
    };

    //Return the public API
    return {
      getUser: getUser,
      registerUser: registerUser,
      login: login,
      getLoggedInUser: getLoggedInUser,
      addPointsToUser: addPointsToUser
    };
  };

})();
