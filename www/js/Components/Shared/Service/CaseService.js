(function() {

  angular.module('app.shared')
    .service('CaseService', CaseService);

  CaseService.$inject = [];

  function CaseService() {
    // For the purpose of this example I will store user data on ionic local storage but you should save it on a database

    var key = 'cases';

    //Base object
    var base = {
      id: 0,

      topic: '', // Should have been an ID instead
      problem: '', // Should have been an ID instead
      imageData: '/img/minby.png',

      locationType: '', // Should have been an ID instead
      address: '',
      location: {
        latitude: 0.00,
        longitude: 0.00
      },

      userId: 0,
      name: '',
      phoneNumber: '',
      email: '',
      feedbackMethod: '', // Should have been an ID instead
      description: '',

      registeredDate: '',
      status: 'Mottatt', // Should have been an ID instead
      upvotes: []
    };


    /**
     * Get all items
     *
     * @returns {Array} List of users
     */

    function getCases() {
      return JSON.parse(window.localStorage.getItem(key) || '[]');
    }


    /**
     * Order cases by most likes
     * @returns {Array} Sorted array of case objects
     */

    function getPopularCases() {
      return _.orderBy(getCases(),['likes'],['ASC']);
    }

    function getCasesOrderByDate() {
      return _.orderBy(getCases(),['id']); //id er en enkel måte å sortere, fikses senere
    }

    function getCasesOrderByLocation() {
      return _.orderBy(getCases(),['address']); //address er en enkel måte å sortere, fikses senere
    }

    /**
     * Get cases connected to the given user
     * @param {number} userId ID of user
     */

    function getCasesForUser(userId) {
      return getCases().filter(function(_case) {
        return _case.userId == userId;
      });
    }


    /**
     * Add an item to the database
     * @param {object} user User object
     */

    function addItem(item) {
      var items = getCases();
      items.push(item);
      window.localStorage.setItem(key,JSON.stringify(items));
    }




    /**
     * Add a new case to the database
     * @param   {object}   newCase Case object
     * @param   {function} error   ErroCallback
     * @returns {object}   new object
     */
    function registerCase(newCase, error) {
      try {
        var _case = _.assign({},base,newCase);
        _case.id = Date.now();
        _case.registeredDate = new Date();

        addItem(_case);

        return _case;
      } catch (err) {
        typeof(error) === 'function' ? error('En ukjent feil oppstod'): '';
      }
    }

    function upvote(_case,userId) {
      var _dbCase = getCase(_case.id);
      var exists = _dbCase.upvotes.indexOf(userId);
      console.log(exists);

      if (exists == -1) {
        _dbCase.upvotes.push(userId);
        return updateCase(_dbCase);
      }
      return _dbCase;
    }

    function updateCase(_case) {
      var newCase = _.assign({},base,_case);
      var cases = getCases().filter(function(src) {
        return src.id != newCase.id;
      });

      cases.push(newCase);
      saveList(cases);
      return newCase;

    }

    function saveList(items) {
      window.localStorage.setItem(key,JSON.stringify(items));
    }

    /**
     * Get all cases
     * @param   {number} id Case ID
     * @returns {object} USER
     */
    function getCase(id){
      return _.find(getCases(), function(_case) {
        return _case.id == id;
      });
    }

    //Return the public API
    return {
      getCases: getCases,
      getCase: getCase,
      registerCase: registerCase,
      getCasesForUser: getCasesForUser,
      getCasesOrderByDate: getCasesOrderByDate,
      getCasesOrderByLocation: getCasesOrderByLocation,
      getPopularCases: getPopularCases,
      upvote: upvote
    };
  };

})();
