angular.module('app.shared')
  .factory('reverseGeocode', ['$q',
    function ($q) {
      return function (latitude, longitude) {
        return $q(function (resolve, reject) {
          var reverseGeocoder = new google.maps.Geocoder();
          var currentPosition = new google.maps.LatLng(latitude, longitude);

          reverseGeocoder.geocode({'latLng': currentPosition}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              resolve(results);
            }
            else {
              reject('Unable to detect your address.');
            }
          });
        });
      }
    }]);
