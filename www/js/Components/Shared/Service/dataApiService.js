angular.module('app.shared')
  .factory('dataApiService', [
    function () {
      return {
        getTopics: getTopics,
        getProblems: getProblems,
        getLocationTypes: getLocationTypes,
        getFeedbackMethods: getFeedbackMethods
      };

      function getTopics() {
        return [
          {id: 1, title: 'Avløp'},
          {id: 2, title: 'Bygg og eiendom'}
        ];
      }

      function getProblems(topicId) {
        var problems = [
          {id: 1, topicId: 1, title: 'Kjemikalie lukt'},
          {id: 2, topicId: 1, title: 'Kloakk lukt'},
          {id: 3, topicId: 1, title: 'Kloakkstopp'},
          {id: 4, topicId: 1, title: 'Kum lokk borte'},
          {id: 5, topicId: 1, title: 'Kum lokk bråker'},
          {id: 6, topicId: 1, title: 'Kum lokk skadet'},
          {id: 7, topicId: 1, title: 'Manglende slamtømming'},
          {id: 8, topicId: 1, title: 'Olje lukt'},
          {id: 9, topicId: 1, title: 'Rotteplage'},
          {id: 10, topicId: 1, title: 'Tilbakeslag'},
          {id: 11, topicId: 1, title: 'Vann i kjeller'},
          {id: 12, topicId: 1, title: 'Vann opp fra kum'},
          {id: 13, topicId: 1, title: 'Vannsig'},
          {id: 14, topicId: 2, title: 'Alarmen hyler'},
          {id: 15, topicId: 2, title: 'Dør oppbrutt'},
          {id: 16, topicId: 2, title: 'Dør skadet'},
          {id: 17, topicId: 2, title: 'Fare for løse gjenstander'},
          {id: 18, topicId: 2, title: 'Flagg ikke heist'},
          {id: 19, topicId: 2, title: 'Flagg ikke tatt ned'},
          {id: 20, topicId: 2, title: 'Forsøpling'},
          {id: 21, topicId: 2, title: 'Lekkasje inne i bygg'},
          {id: 22, topicId: 2, title: 'Lys inne i bygning står på'},
          {id: 23, topicId: 2, title: 'Lys på bygning slukket'},
          {id: 24, topicId: 2, title: 'Nedrøpsrør skadet'},
          {id: 25, topicId: 2, title: 'Ramponert utstyr'},
          {id: 26, topicId: 2, title: 'Renhold, dårlig'},
          {id: 27, topicId: 2, title: 'Skadedyr i bygg'},
          {id: 28, topicId: 2, title: 'Skader og feil, diverse'},
          {id: 29, topicId: 2, title: 'Tagging'},
          {id: 30, topicId: 2, title: 'Vann ut av bygget'},
          {id: 31, topicId: 2, title: 'Vann ut av takrenne'},
          {id: 32, topicId: 2, title: 'Vindu kunst/sprukket'}
        ];

        return problems.filter(function (problem) {
          return problem.topicId === topicId;
        });
      }

      function getLocationTypes() {
        return [
          {id: 1, title: "Gateadresse"},
          {id: 2, title: "Gnr/Bnr"},
          {id: 3, title: "Stedsnavn"},
          {id: 4, title: "Barnehager"},
          {id: 5, title: "Friområder"},
          {id: 6, title: "Idrettsanlegg"},
          {id: 7, title: "Skoler"},
          {id: 8, title: "Sykehjem"},
          {id: 9, title: "Administrasjonsbygg"},
          {id: 10, title: "Helsebygg"}
        ];
      }

      function getFeedbackMethods() {
        return [
          {id: 1, title: 'E-post'},
          {id: 2, title: 'SMS'},
          {id: 3, title: 'Telefon'}
        ];
      }
    }]);
