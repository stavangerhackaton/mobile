angular.module('app.shared')
  .factory('addressFormatterService', [
    function () {
      return {
        getStreet: getStreet
      };

      function getStreet(components) {
        var route = getComponent(components, 'route');
        var street_number = getComponent(components, 'street_number');

        if (route && street_number) {
          return route.long_name + ' ' + street_number.long_name;
        }
      }

      function getComponent(components, type) {
        return _.find(components, function (component) {
          return _.includes(component.types, type);
        })
      }
    }]);
