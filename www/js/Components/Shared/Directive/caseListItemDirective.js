angular.module('app.shared')
	.directive('caseListItem', ['$location',
    function ($location) {
      return {
        retrict: 'EA',
        templateUrl: 'js/Components/Shared/Directive/caseListItemDirective.html',

        scope: {
          model : '=',
          module : '@'
        },
        link: function (scope, element, attrs) {
          scope.model.name = shortName(scope.model.name);

          function shortName(name) {
            if (typeof(name) !== 'string') { return '' }

            var res = name.split(' ');
            if (res.length <= 1) return name;
            var resName = res[0] + ' ' + res[1].substring(0,1) + '.';
            return resName;
          }
          switch (scope.module) {
            case 'profile' :
              scope.url = '#/page1/overviewDetailsProfile/' + scope.model.id;
              break;

            default :
              scope.url = '#/page1/overviewDetails/' + scope.model.id;
              break;
          }
        }
      }
    }]);
