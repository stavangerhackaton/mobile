angular.module('app.overview')
  .controller('OverviewDetailsController', ['$scope', '$stateParams', 'CaseService', 'UserService', '$timeout',
    function ($scope, $stateParams, caseService, userService, $timeout) {
      console.log('OverviewDetailsController loaded (' + $stateParams.caseId + ')');


      $scope.hasVoted = false;
      $scope.mapLink = "#/page1/map/" + $stateParams.caseId;

      $scope.upvote = function () {
        //$scope.model.upvotes++;
        $scope.hasVoted = true;
        userService.getLoggedInUser(function (user) {
          $timeout(function() {
            var newCase = caseService.upvote($scope.model, user.id);
            console.log('NewCase: ', newCase);
            if (newCase != -1) {
              $scope.model = newCase;
            }

            $scope.model.name = shortName($scope.model.name);
          });
        });

      };


      function shortName(name) {
        if (typeof(name) !== 'string') { return '' }

        var res = name.split(' ');
        if (res.length <= 1) return name;
        var resName = res[0] + ' ' + res[1].substring(0,1) + '.';
        return resName;
      }

      function init() {
        $scope.model = caseService.getCase($stateParams.caseId);
        $scope.model.name = shortName($scope.model.name);

        console.log($scope.model);
      };

      init();


    }]);
