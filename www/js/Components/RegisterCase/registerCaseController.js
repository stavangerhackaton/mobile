angular.module('app.registerCase')
  .controller('RegisterCaseController', ['$scope', '$cordovaCamera', 'selectLocationModal', 'dataApiService', 'CaseService', 'UserService', 'addressFormatterService',
    function ($scope, $cordovaCamera, selectLocationModal, dataApiService, CaseService, UserService, addressFormatterService) {
      console.log('RegisterCaseController loaded');
      $scope.$on('$ionicView.beforeEnter', function() {
        UserService.getLoggedInUser(function (user) {
          $scope.isLoggedIn = true;
        });
      })



      resetItem();

      $scope.topics = dataApiService.getTopics();

      $scope.$watch('newItem.selectedTopic', function (selectedTopic) {
        if (selectedTopic) {
          $scope.problems = dataApiService.getProblems(selectedTopic.id);
        }
      });

      $scope.locationTypes = dataApiService.getLocationTypes();

      $scope.feedbackMethods = dataApiService.getFeedbackMethods();

      $scope.newItem.selectedTopic = {};
      $scope.newItem.selectedProblem = {};
      $scope.newItem.selectedLocationType = {};
      $scope.newItem.selectedFeedbackMethod = {};
      $scope.newItem.imageData = '';

      $scope.addPicture = function() {
        if (!navigator.camera) {
          alert("Camera API not supported", "Error");
          return;
        }

        var options = {
          quality: 50,
          destinationType: Camera.DestinationType.DATA_URL,
          sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
          encodingType: 0     // 0=JPG 1=PNG
        };

        $cordovaCamera.getPicture(options).then(
          function(imgData) {
            $scope.newItem.imageData = 'data:image/jpg;base64,' + imgData;
          },
          function() {
            //alert('Error taking picture', 'Error');
          }
        );

        return false;
      };

      $scope.addLocation = function() {
        selectLocationModal.show(function (result) {
          if (result) {
            var address = addressFormatterService.getStreet(result.address_components);
            console.log('Selected address:' + address);

            $scope.newItem.address = address;

            $scope.newItem.location = {
              latitude: result.geometry.location.lat(),
              longitude: result.geometry.location.lng()
            };
          }
        });
      };

      $scope.submit = function() {
        console.log('Submitting data to backend');

        UserService.getLoggedInUser(function (user) {
          registerCase(user)
        }, function (error) {
          registerCase(undefined, error);
        });

        function registerCase(user, error) {
          var _case = {
            topic: $scope.newItem.selectedTopic.title,
            problem: $scope.newItem.selectedProblem.title,
            description: $scope.newItem.description,
            locationType: $scope.newItem.selectedLocationType.title,
            address: $scope.newItem.address,
            location: $scope.newItem.location
          };
          console.log('ImageData: ',$scope.newItem.imageData);

          if ($scope.newItem.imageData != '') {
            _.assignIn(_case, {
              imageData: $scope.newItem.imageData,
            });
          }

          if (user) {
            _.assignIn(_case, {
              userId: user.id,
              name: user.name
            });
            UserService.addPointsToUser(user.id,100);
          }
          else {
            _.assignIn(_case, {
              name: $scope.newItem.name,
              phoneNumber: $scope.newItem.phoneNumber,
              email: $scope.newItem.email,
              feedbackMethod: $scope.newItem.selectedFeedbackMethod.title
            });
          }

          var registeredCase = CaseService.registerCase(_case);

          resetItem();
        }
      };

      function resetItem() {
        $scope.newItem = {
          selectedTopic: {},
          selectedProblem: {},
          selectedLocationType: {},
          selectedFeedbackMethod: {}
        };
      }
    }]);
