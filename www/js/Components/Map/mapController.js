angular.module('app.map')
  .controller('MapController', ['$scope', '$cordovaGeolocation', '$state', 'CaseService','$stateParams',
    function ($scope, $cordovaGeolocation, $state, CaseService, $stateParams) {
      console.log("MapController åpnet");

      var caseId = $stateParams.caseId;

      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
          var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          var mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          var map = new google.maps.Map(document.getElementById('map'), mapOptions);

          google.maps.event.addListenerOnce(map, 'idle', function(){
            if(caseId) {
              var _case = CaseService.getCase($stateParams.caseId);
              if (_case.location) {
                placeMarker(_case.location.latitude, _case.location.longitude, _case.id);
              }

            } else {
              CaseService.getCases().forEach(function (_case) {
                if (_case.location) {
                  placeMarker(_case.location.latitude, _case.location.longitude, _case.id);
                }
              });
            }

            
          });

          function placeMarker(latitude, longitude, id) {
            var url = $state.href('tabsController.overviewDetails', {caseId: id});

            var marker = new google.maps.Marker({
              map: map,
              position: new google.maps.LatLng(latitude, longitude),
              url: url
            });

            google.maps.event.addListener(marker, 'click', function() {
              window.location.href = this.url;
            });
          }

          $scope.map = map;
        }, function(err) {
          console.log(err);
        });
    }]);
