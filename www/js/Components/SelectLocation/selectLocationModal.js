angular.module('app.selectLocation')
  .factory('selectLocationModal', ['$ionicModal', '$rootScope', '$controller',
    function($ionicModal, $rootScope, $controller) {
      return {
        show: show
      };

      function show(callback) {
        var scope = $rootScope.$new();

        scope.hideModal = function (value) {
          scope.modal.remove();

          callback(value);
        };

        return $ionicModal.fromTemplateUrl('js/Components/SelectLocation/selectLocation.html', {
          scope: scope
        }).then(function(modal) {
          scope.modal = modal;
          scope.modal.show();

          $controller('SelectLocationController', { $scope: scope });

          return modal;
        });
      }
    }]);
