angular.module('app.selectLocation')
  .controller('SelectLocationController', ['$scope', '$cordovaGeolocation', 'reverseGeocode', 'addressFormatterService',
    function ($scope, $cordovaGeolocation, reverseGeocode, addressFormatterService) {
      console.log('SelectLocationController loaded');

      $scope.inputs = {};

      // Initial loading of map
      getCurrentLocation(function (position) {
        console.log('Found initial location');

        var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        google.maps.event.addListener(map, 'click', function(event) {
          placeMarker(event.latLng);
        });

        $scope.map = map;
      });

      $scope.getCurrentLocation = function () {
        getCurrentLocation(function (position) {
          console.log('Got current location');

          var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          placeMarker(latLng)
        });
      };

      $scope.cancel = function () {
        console.log('Canceled');
        $scope.hideModal();
      };

      $scope.save = function () {
        console.log('Saved');
        $scope.hideModal($scope.result);
      };

      var previousMarker;
      function placeMarker(position) {
        console.log('Placing marker');

        // Clear previous marker
        if (previousMarker) {
          previousMarker.setMap(null);
        }

        previousMarker = new google.maps.Marker({
          map: $scope.map,
          position: position,
          animation: google.maps.Animation.DROP
        });

        reverseGeocode(position.lat(), position.lng())
          .then(function (results) {
            if (results[0]) {
              $scope.inputs.address = addressFormatterService.getStreet(results[0].address_components);
              $scope.result = results[0];
            }
          }, function () {
            alert('Unable to detect your address.');
          });
      }

      function getCurrentLocation(callback) {
        console.log('Getting current location');

        $scope.locating = true;

        var geolocationOptions = {timeout: 10000, enableHighAccuracy: false};
        $cordovaGeolocation
          .getCurrentPosition(geolocationOptions)
          .then(function (position) {
            console.log('Found location: ' + position.coords.latitude + ',' + position.coords.longitude);

            $scope.locating = false;

            currentLocation = position;

            callback(position);
          }, function (err) {
            alert('Failed to get current position.');
          });
      }
    }]);
