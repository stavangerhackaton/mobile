angular.module('app.overview')
  .controller('OverviewController', ['$scope', '$ionicLoading', 'PullToRefreshService', 'CaseService',
    function ($scope, $ionicLoading, pullToRefreshService, CaseService) {
    $scope.cases = [];
    $scope.filter = "popular";

    $scope.updateFilter = function (filter) {
      $scope.filter = filter;
      pullToRefreshService.triggerPtr('pullToRefresh-content');
    };

    $scope.refresh = function () {
      $scope.getData();
      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.getData = function () {
      var filter = $scope.filter;
      var cases = [];
      if(filter && filter === "popular") {
        cases = CaseService.getPopularCases();
      } else if(filter && filter === "newest") {
        cases = CaseService.getCasesOrderByDate();
      }else if(filter && filter === "closest") {
        cases = CaseService.getCasesOrderByLocation();
      }
      else {
        cases = CaseService.getCases();
      }
      
      $scope.cases = cases;
    };

    //bør endres slik at en bare refresher hver gang en legger til caser
    $scope.$on('$ionicView.beforeEnter', function() {
      init();
    });

    function init() {
      pullToRefreshService.triggerPtr('pullToRefresh-content');
    };
    init();
  }]);
